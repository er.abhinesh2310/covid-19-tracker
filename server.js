const express = require('express');
const path = require('path');
const app = express();
const cors = require('cors');

//handling CORS...
app.use(cors());

// Sending Static files ...
app.use(express.static(__dirname));
app.use(express.static(path.join(__dirname, 'build')));
app.get('/*', function (req, res) {
    res.sendFile(path.join(__dirname, 'build', 'index.html'));
});

app.use(function ignoreFavicon(_req, _res, _next) {
    _req.originalUrl === '/favicon.ico'
        ? _res.status(204).json({ nope: true })
        : _next();
});

//handling errors...
app.use((_req, _res, _next) => {
    const error = new Error('Not Found!');
    error.status = 404;
    _next(error);
});

app.use((error, _req, _res, _next) => {
    _res.status(error.status || 500)
        .json({
            responseCode: error.status || 500,
            message: 'Invalid routes ...',
            error: error.message
        });
});

const PORT = process.env.PORT || 9000;
app.listen(PORT, () => {
    console.log(`Server Listening at ${PORT}`);
});